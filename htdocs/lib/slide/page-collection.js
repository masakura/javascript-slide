import Page from './page.js';

export default class PageCollection {
  /**
   * @param {Page[]} pages
   */
  constructor(pages) {
    this.pages = pages;
  }

  /**
   * @param {number} index
   * @return {number}
   */
  show(index) {
    if (index < 0) {
      index = 0;
    } else if (index >= this.pages.length) {
      index = this.pages.length - 1;
    }

    this.pages.forEach(page => page.hide());
    this.pages[index].show();

    return index;
  }

  /**
   * @param {HTMLElement[]|NodeListOf<Element>} elements
   * @return {PageCollection}
   */
  static from(elements) {
    const pages = Array.from(elements).map(element => new Page(element));
    return new PageCollection(pages);
  }
}
