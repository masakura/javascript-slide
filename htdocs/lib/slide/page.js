export default class Page {
  /**
   * @param {HTMLElement} element
   */
  constructor(element) {
    this.element = element;
  }

  show() {
    this.element.classList.add('shown');
  }

  hide() {
    this.element.classList.remove('shown');
  }
}
