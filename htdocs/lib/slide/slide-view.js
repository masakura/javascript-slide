export default class SlideView {
  /**
   * @param {PageCollection} pages
   */
  constructor(pages) {
    this.pages = pages;
    this.current = 0;
    this.update();
  }

  update() {
    this.current = this.pages.show(this.current);
  }

  first() {
    this.current = 0;
    this.update();
  }

  last() {
    this.current = Number.MAX_VALUE;
    this.update();
  }

  next() {
    this.current += 1;
    this.update();
  }

  previous() {
    this.current -= 1;
    this.update();
  }
}
