import PageCollection from './lib/slide/page-collection.js';
import SlideView from './lib/slide/slide-view.js';
import pagesBuilder from './pages/index.js';

document.addEventListener('DOMContentLoaded', () => {
  const $slide = document.querySelector('.slide');
  Array.from(pagesBuilder.build())
    .forEach((content) => {
      const article = document.createElement('article');
      article.classList.add('page');
      article.appendChild(content);
      $slide.appendChild(article);
    });
  const pages = PageCollection.from(document.querySelectorAll('.page'));
  const slide = new SlideView(pages);

  window.addEventListener('keydown', (e) => {
    switch (e.key) {
      case 'ArrowRight':
        slide.next();
        break;

      case 'ArrowLeft':
        slide.previous();
        break;

      case 'Home':
        slide.first();
        break;

      case 'End':
        slide.last();
        break;

      default:
        // do nothing
    }
  });
});
