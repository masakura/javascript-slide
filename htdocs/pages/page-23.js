import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  object1: `
var map = {
  key1: 'value1',
  toString: 'value2', // これは NG
};

alert(map.key1);

try {
  // toString は関数ではなくなった
  alert(\`\${map}\`);
} catch (e) {
  alert(e);
}
`.trim(),
  map1: `
var map = new Map();
map.set('key1', 'value1');
map.set('toString', 'value2');

alert(map.get('key1'));
alert(map.get('toString'));
`.trim(),
  object2: `
var key1 = new Object();
var key2 = new Object();

var map = {};
 // キーは暗黙で文字列に変換される
map[key1] = 'value1';
map[key2] = 'value2';

alert(map[key1]);
alert(map[key2]);
`.trim(),
  map2: `
var key1 = new Object();
var key2 = new Object();

var map = new Map();
map.set(key1, 'value1');
map.set(key2, 'value2');

alert(map.get(key1));
alert(map.get(key2));
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];

        const $button = code.querySelector('button.run')
          .addEventListener('click', () => {
            // eslint-disable-next-line
            eval(codes[type]);
          });
      });
  }

  template() { // eslint-disable-line
    return `
<style>
  .samplem .sample > *:first-child {
    flex-grow: unset;
    width: 26em;
  }
</style>

<h3>Native JavaScript (Map)</h3>

<div class="samplem">
  <div class="sample">
    <div class="code" data-type="object1">
      <h5 class="title">Object</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="map1">
      <h5 class="title">Map</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
  <div class="sample">
    <div class="code" data-type="object2">
      <h5 class="title">Object</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="map2">
      <h5 class="title">Map (Object key)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
