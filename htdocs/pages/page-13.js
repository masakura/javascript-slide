import contents from './content-collection.js';

contents.define(`
<h3>jQuery はいらなくなったのか?</h3>
<ul>
  <li>ブラウザーごとに関数が違うとかはほとんどなくなった</li>
  <li>ブラウザーによっては標準未実装があるが、Polyfill やトランスパイラに任せる</li>
  <li>アニメーションは CSS を強く推奨!</li>
</ul>
`);
