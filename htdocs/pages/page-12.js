import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  javascript: `
var prev = Date.now();
var opacity = 0;
var element = document
  .querySelector('.sample05 .javascript .icon');

function animate() {
  requestAnimationFrame(animate);

  var now = Date.now();
  var step = now - prev;
  prev = now;
  opacity += step / 1000;
  
  if (opacity > 1) opacity = 0;
  element.style.opacity = opacity;
}

animate();
`.trim(),
  jquery: `
function animate() {
  $('.sample05 .jquery .icon')
    .fadeIn(0, 'linear')
    .fadeOut(1000, 'linear', animate);
}

animate();
`.trim(),
  css: `
@keyframes sample05animation {
  0% { opactiy: 0; }
  100% { opacity: 1; }
}

.sample05 .css .icon {
  opacity: 0;
  animation-name: sample05animation;
  animation-duration: 1s;
  animation-timing-function: linear;
  animation-iteration-count: infinite
}
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelector('.halt')
      .addEventListener('click', () => {
        const end = Date.now() + 1000 * 3;
        // eslint-disable-next-line
        while (Date.now() <= end) {}
      });

    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('pre code');
        $code.textContent = codes[code.dataset.type];

        if (code.dataset.type !== 'css') {
          // eslint-disable-next-line
          eval(codes[code.dataset.type]);
        } else {
          // eslint-disable-next-line
          code.innerHTML = `
<style>
${codes[code.dataset.type]}
</style>

${code.innerHTML}
`;
        }
      });
  }

  template() { // eslint-disable-line
    return `
<style>
  .sample05 .buttons button {
    width: 100%;
  }
  
  .sample05 .icon {
    font-size: 300%;
  }
</style>

<h3>アニメーション</h3>

<div class="sample05">
  <div class="buttons">
    <button class="halt">
      ブラウザーを 3 秒だけ停止させる
    </button>
  </div>

  <div class="sample">
    <div class="code javascript" data-type="javascript">
      <h5 class="title">JavaScript</h5>
      <div class="navigation">
        <div class="icon">@</div>    
      </div>
      <pre><code class="javascript"></code></pre>
    </div>
    <div class="code jquery" data-type="jquery">
      <h5 class="title">jQuery</h5>
      <div class="navigation">
        <div class="icon">@</div>    
      </div>
      <pre><code class="javascript"></code></pre>
    </div>
    <div class="code css" data-type="css">
      <div class="inner"></div>
      <h5 class="title">CSS Animation</h5>
      <div class="navigation">
        <div class="icon">@</div>    
      </div>
      <pre><code class="css"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
