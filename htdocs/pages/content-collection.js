import PageContentElement from './page-content.js';

const getIndexName = (index) => {
  const name = `00${index}`;
  return name.substring(name.length - 2);
};

const getTagName = index => `page-${getIndexName(index)}`;

class ContentCollection {
  constructor() {
    this.tagNames = [];
  }

  define(template) {
    const tagName = getTagName(this.tagNames.length + 1);
    PageContentElement.define(tagName, template);
    this.tagNames.push(tagName);
  }
}

export default new ContentCollection();
