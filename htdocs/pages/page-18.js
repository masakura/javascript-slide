import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  old1: `
function Hoge(name) {
  this.name = name;
}

Hoge.prototype.show = function () {
  alert(this.name);
}

function Fuga() {
  Hoge.call(this, 'fuga');
}

Object.setPrototypeOf(Fuga.prototype, Hoge.prototype);

var fuga = new Fuga();
fuga.show();
`.trim(),
  new1: `
class Hoge {
  constructor(name) {
    this.name = name;
  }
  
  show() {
    alert(this.name);
  }
}

class Fuga extends Hoge {
  constructor() {
    super('fuga');
  }
}

var fuga = new Fuga();
fuga.show();
`.trim(),
};

codes.html2 = codes.html;
codes.html4 = codes.html3;
codes.html6 = codes.html5;

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];

        code.querySelector('button.run')
          .addEventListener('click', () => {
            // eslint-disable-next-line
            eval(codes[type]);
          });
      });
  }

  template() { // eslint-disable-line
    return `
<style>
</style>

<h3>Native JavaScript (class syntax sugar)</h3>

<div>
  <div class="sample">
    <div class="code" data-type="old1">
      <h5 class="title">Native JavaScript (昔)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="new1">
      <h5 class="title">Native JavaScript (class syntax sugar)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
