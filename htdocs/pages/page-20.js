import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  old1: `
var text = 'hoge';

function hello(condition) {
  alert(text); // undefined

  if (condition) {
    var text = 'hello!';
    alert(text);
  }
}

hello(true);
`.trim(),
  new1: `
const text = 'hoge';

function hello(condition) {
  alert(text); // hoge
  
  if (condition) {
    const text = 'hello!';
    alert(text);
  }
}

hello(true);
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];

        code.querySelector('button.run')
          .addEventListener('click', () => {
            // eslint-disable-next-line
            eval(codes[type]);
          });
      });
  }

  template() { // eslint-disable-line
    return `
<style>
</style>

<h3>Native JavaScript (const / let)</h3>

<div>
  <div class="sample">
    <div class="code" data-type="old1">
      <h5 class="title">Native JavaScript (昔)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="new1">
      <h5 class="title">Native JavaScript (const / let)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
