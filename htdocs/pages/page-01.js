import contents from './content-collection.js';

contents.define(`
<style>
  .self-intro table {
    position: absolute;
    top: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 90%;
    height: 100%;
  }
  
  .self-intro th {
    padding-right: 2em;
  }
</style>

<div class="self-intro">
  <h3>自己紹介</h3>

  <table>
    <tr>
      <th>名前</th>
      <td>政倉 智 (まさくら とも)</td>    
    </tr>
    <tr>
      <th>所属</th>
      <td>codeArts 株式会社</td>
    </tr>
    <tr>
      <th>所属</th>
      <td>html5j 鹿児島</td>
    </tr>
    <tr>
      <th>好きなもの</th>
      <td>C#</td>    
    </tr>
  </table>
</div>
`);
