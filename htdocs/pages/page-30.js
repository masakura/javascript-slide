import contents from './content-collection.js';

contents.define(`
<h3>ES2015</h3>
<ul>
  <li>ECMAScript 6th Edition の 2015 年度リリース</li>
  <li>これ以降毎年 6 月に規格が策定されることに</li>
  <li>いっぱい追加されたので詳細は割愛</li>
  <li>ES2016 以降は <a href="https://github.com/tc39/proposals/blob/master/finished-proposals.md">Finished Proposals
</a> を</li>
  <li>
    実装状況は <a href="http://kangax.github.io/compat-table/es6/">ES6 compatibility table</a>
    と <a href="http://kangax.github.io/compat-table/es2016plus/">ES2016+ compatibility table</a>
  </li>
</ul>
`);
