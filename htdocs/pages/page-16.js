import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  old1: `
function total() {
  // 可変長引数の arguments 変数は配列ではないので
  // 配列に変換しないと reduce が使えない
  var args = Array.prototype.slice
    .call(arguments);
  
  return args.reduce(function (prev, current) {
    return prev + current;
  }, 0);
}

alert(total(1, 2, 3, 4, 5));
`.trim(),
  old2: `
function total() {
  var args = Array.from(arguments);
  
  return args.reduce(function (prev, current) {
    return prev + current;
  }, 0);
}

alert(total(1, 2, 3, 4, 5));
`.trim(),
  new1: `
function total(...args) {
  return args.reduce(function (prev, current) {
    return prev + current;
  }, 0);
}

alert(total(1, 2, 3, 4, 5));
`.trim(),
};

codes.html2 = codes.html;
codes.html4 = codes.html3;
codes.html6 = codes.html5;

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];

        code.querySelector('button.run')
          .addEventListener('click', () => {
            // eslint-disable-next-line
            eval(codes[type]);
          });
      });
  }

  template() { // eslint-disable-line
    return `
<style>
</style>

<h3>Native JavaScript (可変長引数)</h3>

<div>
  <div class="sample">
    <div class="code" data-type="old1">
      <h5 class="title">Native JavaScript (昔)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="old2">
      <h5 class="title">Native JavaScript (Array.from)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="new1">
      <h5 class="title">Native JavaScript (Rest parameters)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
