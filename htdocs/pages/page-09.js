import contents from './content-collection.js';
import PageContentElement from './page-content.js';

/*
 jQuery 1.7.0 attachEvent & addEventListner
 jQuery 1.4.0 attachEvent & addEventListner
 jQuery 1.3.0 attachEvent & addEventListner
 jQuery 1.2.0 attachEvent & addEventListner
 jQuery 1.1.0 onclick =
 jQuery 1.0.0 onclick =
 */

const codes = {
  old: `
function handler() {
  alert('原始的なやり方');
}

var prevHandler = button.onclick;
button.onclick = function (e) {
  // もともと設定されているハンドラーを実行
  if (prevHandler) prevHandler(e);
  
  // 今回のイベントを実行
  handler(e);
};
`.trim(),
  ie: `
function handler() {
  alert('IE 系');
}

button.attachEvent('onclick', handler);
`.trim(),
  dom: `
function handler() {
  alert('DOM Level 2');
}

button.addEventListener('click', handler);
`.trim(),
  jquery: `
function handler() {
  alert('jQuery');
}

$(button).on('click', handler);
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('pre code');
        $code.textContent = codes[code.dataset.type];

        // eslint-disable-next-line
        const button = code.querySelector('button.run');
        if (button) {
          // eslint-disable-next-line
          eval(codes[code.dataset.type]);
        }
      });
  }

  template() { // eslint-disable-line
    return `
<style>
</style>

<h3>イベントの登録</h3>

<div class="sample03 sample">
  <div class="code" data-type="old">
    <h5 class="title">Native JavaScript (原始的)</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="ie">
    <h5 class="title">IE 系 (今では IE でも使えません)</h5>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="dom">
    <h5 class="title">Native JavaScript (DOM Level 2)</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="jquery">
    <h5 class="title">jQuery</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
</div>

<h4>jQuery でのサポート</h4>
<ul>
  <li>1.2.0 で <code>attachEvent</code> / <code>addEventListener</code> をサポート</li>
  <li>2.0.0 で <code>attachEvent</code> のサポートを終了 (IE6 / 7 / 8 のサポート終了)</li>
</ul>
`;
  }
}

contents.define(Page);
