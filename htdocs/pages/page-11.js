import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  ie: `
var xhr = new ActiveXObject("Microsoft.XMLHTTP");

xhr.onreadystatechange = function () {
  switch (xhr.readyState) {
    case 4:
      if (xhr.status === 200) {
        alert(xhr.responseText);
      }
  }
};

xhr.open('GET', 'data.json', false);
xhr.send(null);
`.trim(),
  xhr: `
var xhr = new XMLHttpRequest();

xhr.onreadystatechange = function () {
  switch (xhr.readyState) {
    case 4:
      if (xhr.status === 200) {
        alert(xhr.responseText);
      }
  }
};

xhr.open('GET', 'data.json', false);
xhr.send(null);
`.trim(),
  jquery: `
$.ajax({
  url: 'data.json',
  dataType: 'text',
  success: function (data) {
    alert(data);
  },
});
`.trim(),
  jqueryDeferred: `
$.ajax({
  url: 'data.json',
  dataType: 'text',
})
  .done(function (data) {
    alert(data);
  });
`.trim(),
  fetch: `
fetch('data.json')
  .then(function (response) {
    if (response.status === 200) {
      return response.text();
    } else {
      return Promise.reject();
    }
  })
  .then(function (data) {
    alert(data);
  });
`.trim(),
  fetchAsyncAwait: `
var response = await fetch('data.json');
if (response.status === 200) {
  var text = await response.text();
  alert(text);
}
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('pre code');
        $code.textContent = codes[code.dataset.type];

        const $button = code.querySelector('button.run');
        if ($button) {
          $button.addEventListener('click', () => {
            let script = codes[code.dataset.type];
            script = `(async function () {\n${script}`;
            script += '\n})();';
            // eslint-disable-next-line
            eval(script);
          });
        }
      });
  }

  template() { // eslint-disable-line
    return `
<style>
</style>

<h3>非同期通信</h3>

<div class="sample04 sample">
  <div class="code" data-type="ie">
    <h5 class="title">IE 5 (今では IE でも使えません)</h5>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="xhr">
    <h5 class="title">XMLHttpRequest</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="jquery">
    <h5 class="title">jQuery (old style)</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="jqueryDeferred">
    <h5 class="title">jQuery (Deferred)</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="fetch">
    <h5 class="title">Fetch API (Promise)</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
  <div class="code" data-type="fetchAsyncAwait">
    <h5 class="title">Fetch API (Async / Await)</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="javascript"></code></pre>
  </div>
</div>

<h4>jQuery でのサポート</h4>
<ul>
  <li>1.0.0 で <code>Microsoft.XMLHTTP</code> / <code>XMLHttpRequest</code> をサポート</li>
  <li>2.0.0 で <code>Microsoft.XMLHTTP</code> のサポートを終了 (IE6 / 7 / 8 のサポート終了)</li>
  <li>3.3.1 時点で <code>fetch</code> は使ってません</li>
</ul>
`;
  }
}

contents.define(Page);
