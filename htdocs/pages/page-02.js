import contents from './content-collection.js';

contents.define(`
<h3>アジェンダ</h3>
<ul>
  <li>jQuery のお話</li>
  <li>ECMAScript 6th Edition の紹介</li>
  <li>ES6 Modules</li>
</ul>
`);
