import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  html: `
<ul class="list1">
  <li>いち</li>
  <li>にい</li>
  <li>さん</li>
</ul>
  `.trim(),
  html3: `
<ul class="list2">
  <li>いち</li>
  <li>にい</li>
  <li>さん</li>
</ul>
  `.trim(),
  html5: `
  <ul class="list3">
  <li>いち</li>
  <li>にい</li>
  <li>さん</li>
</ul>
    `.trim(),
  delegate: `
$('.sample06 .list3').on('click', 'li', function () {
  alert($(this).text());
});
`.trim(),
  multiple: `
$('.sample06 .list2 li').on('mousedown mouseup', function (e) {
  $(this).css({ 'font-weight': e.buttons > 0 ? 'bold' : 'normal' });
});
`.trim(),
  all: `
$('.sample06 .list1 li').on('click', function () {
  alert($(this).text());
});
`.trim(),
};

codes.html2 = codes.html;
codes.html4 = codes.html3;
codes.html6 = codes.html5;

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        if (type === 'html2' || type === 'html4' || type === 'html6') {
          $code.innerHTML = codes[type];
        } else {
          $code.textContent = codes[type];
        }

        if (type === 'delegate' || type === 'multiple' || type === 'all') {
          // eslint-disable-next-line
          eval(codes[code.dataset.type]);
        }
      });
  }

  template() { // eslint-disable-line
    return `
<style>
  .sample06 .sample > .html {
    width: 16em;
  }
  
  .sample06 .sample > .previe {
    width: 10em;
  }
  
  .sample06 .sample > * {
    flex-grow: unset;
  }
  
  .sample06 .sample > .javascript {
    flex-grow: 100;
  }
</style>

<h3>jQuery の短く書けるは結構すごい</h3>

<div class="sample06">
  <div class="sample">
    <div class="code html" data-type="html">
      <h5 class="title">HTML (Source)</h5>
      <pre><code class="html inner"></code></pre>
    </div>
    <div class="code preview" data-type="html2">
      <h5 class="title">HTML (Preview)</h5>
      <div class="inner"></div>
    </div>
    <div class="code javascript" data-type="all">
      <h5 class="title">jQuery (multiple elements)</h5>
      <div class="navigation">
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
  <div class="sample">
    <div class="code html" data-type="html3">
      <h5 class="title">HTML (Source)</h5>
      <pre><code class="html inner"></code></pre>
    </div>
    <div class="code preview" data-type="html4">
      <h5 class="title">HTML (Preview)</h5>
      <div class="inner"></div>
    </div>
    <div class="code javascript" data-type="multiple">
      <h5 class="title">jQuery (multi event)</h5>
      <div class="navigation">
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
  <div class="sample">
    <div class="code html" data-type="html5">
      <h5 class="title">HTML (Source)</h5>
      <pre><code class="html inner"></code></pre>
    </div>
    <div class="code preview" data-type="html6">
      <h5 class="title">HTML (Preview)</h5>
      <div class="inner"></div>
    </div>
    <div class="code javascript" data-type="delegate">
      <h5 class="title">jQuery (delegate event)</h5>
      <div class="navigation">
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
