import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  old1: `
var now = new Date();

alert('今は ' + now + ' です'); 
`.trim(),
  new1: `
var now = new Date();

alert(\`今は \${now} です\`);
`.trim(),
  old2: `
alert(
  '一行目\\n' +
  '二行目\\n' +
  '三行目'
);
`.trim(),
  new2: `
alert(
\`一行目
二行目
三行目\`
);
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];

        code.querySelector('button.run')
          .addEventListener('click', () => {
            // eslint-disable-next-line
            eval(codes[type]);
          });
      });
  }

  template() { // eslint-disable-line
    return `
<style>
  .samplet .sample > * {
    flex-grow: unset;
    width: 50%;
  }
</style>

<h3>Native JavaScript (template literal)</h3>

<div class="samplet">
  <div class="sample">
    <div class="code" data-type="old1">
      <h5 class="title">Native JavaScript (昔)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="new1">
      <h5 class="title">Native JavaScript (template literal)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
  <div class="sample">
    <div class="code" data-type="old2">
      <h5 class="title">Native JavaScript (昔)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="new2">
      <h5 class="title">Native JavaScript (template literal)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
