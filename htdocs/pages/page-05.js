import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  html: `
<ul id="names">
  <li>
    氏名: <span>ほげ太郎</span><br>年齢: <span>15</span>
  </li>
  <li>
    氏名: <span>ふが次郎</span><br>年齢: <span>18</span>
  </li>
  <li>
    氏名: <span>ぴよ花子</span><br>年齢: <span>21</span>
  </li>
</ul>
`.trim(),
  native: `
var text = '';

var lis = document
  .getElementById('names')
  .getElementsByTagName('li');

for (var i = 0; i < lis.length; i++) {
  text += lis[i]
    .getElementsByTagName('span')[0]
    .innerHTML + '\\n';
}

alert(text);
`.trim(),
  jquery: `var text = '';

var names = $('#names > li > span:first-child');

for (var i = 0; i < names.length; i++) {
  text += names.eq(i).text() + '\\n';
}

alert(text);
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelector('.names').innerHTML = codes.html;

    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('pre code');
        $code.textContent = codes[code.dataset.type];

        const $button = code.querySelector('button.run');
        if ($button) {
          $button.addEventListener('click', () => {
            // eslint-disable-next-line
            eval(codes[code.dataset.type]);
          });
        }
      });
  }

  template() { // eslint-disable-line
    return `
<style>
  .samplea .html {
    width: 100%;
  }
</style>

<h3>要素の選択</h3>

<div class="samplea sample">
  <div class="html code" data-type="html">
    <h5 class="title">HTML</h5>
    <pre><code class="sample-source html"></code></pre>
  </div>
  <div class="javascript code" data-type="native">
    <h5 class="title">Native JavaScript (old style)</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="native-source javascript"></code></pre>
  </div>
  <div class="javascript code" data-type="jquery">
    <h5 class="title">jQuery</h5>
    <div class="navigation">
      <button class="run">実行</button>
    </div>
    <pre><code class="jquery-source javascript"></code></pre>
  </div>
  <div style="display: none" class="names"></div>
</div>
`;
  }
}

contents.define(Page);
