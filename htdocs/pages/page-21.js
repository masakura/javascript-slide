import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  old1: `
var array = [1, 2, 3, 4, 5];

var total = 0;

for (var i = 0; i < array.length; i++) {
  total += array[i];
}

alert(total);
`.trim(),
  old2: `
var array = [1, 2, 3, 4, 5];

var total = 0;

for (var value in array) {
  total += value;
}

alert(total);
`.trim(),
  old3: `
var array = [1, 2, 3, 4, 5];
array.text = 10;

var total = 0;

for (var index in array) {
  total += array[index];
}

alert(total);
`.trim(),
  new1: `
const array = [1, 2, 3, 4, 5];
array.text = 10;

let total = 0;

for (const value of array) {
  total += value;
}

alert(total);
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];

        code.querySelector('button.run')
          .addEventListener('click', () => {
            // eslint-disable-next-line
            eval(codes[type]);
          });
      });
  }

  template() { // eslint-disable-line
    return `
<style>
</style>

<h3>Native JavaScript (for of)</h3>

<div>
  <div class="sample">
    <div class="code" data-type="old1">
      <h5 class="title">Native JavaScript (昔)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="old2">
      <h5 class="title">Native JavaScript (NG)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="old3">
      <h5 class="title">Native JavaScript (NG)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code" data-type="new1">
      <h5 class="title">Native JavaScript (for of)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
