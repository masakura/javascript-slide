import contents from './content-collection.js';

contents.define(`
<h3>要素の選択</h3>
<ol>
  <li>
      初期の JavaScript はフォーム・画像・リンクくらいしかアクセスできなかった。
  </li>
  <li>
    JavaScript で本格的にアプリを書こうとする人 (Ex: DHTML) が現れるも、力不足なため拡張される。あらゆる HTML 要素にアクセスできるように。
  </li>
  <li>
    標準仕様として DOM Level 1 が登場。(<code>getElementBy</code> 系)
  </li>
  <li>
    低レベル API は利用が面倒なので、jQuery や Prototype などが生まれる。
  </li>
  <li>
    jQuery Selector が有用だと認められ、Selector API が登場。(<code>querySelector</code> 系)
  </li>
  <li>
    jQuery 1.3.0 から <code>querySelectorAll</code> を使うようになり、高速化する。
</li>
</ol>
`);
