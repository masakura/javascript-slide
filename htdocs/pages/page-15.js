import contents from './content-collection.js';

contents.define(`
<h3>jQuery はいらなくなったのか?</h3>
<ul>
  <li>jQuery のメリットは少なくなったとはいえ、依然として強力</li>
  <li>自分の場合は、Native JavaScript か Vue.js かっていう感じで、jQuery はほぼ使ってない</li>
  <li>jQuery や Native JavaScript でアプリを作るのはあまりおすすめしない</li>
  <li>機会があれば Vue.js や Angular も使ってみてください (最小構成は jQuery と遜色ないくらいコンパクト)</li>
</ul>
`);
