import contents from './content-collection.js';

contents.define(`
<h3>Q. jQuery を今でも使う理由は? (現役な人)</h3>

<p>(複数回答可)</p>

<ul>
  <li>なんとなく</li>
  <li>短く書けるから</li>
  <li>クロスブラウザー対応のため</li>
</ul>
`);
