import contents from './content-collection.js';
import './page-00.js';
import './page-01.js';
import './page-02.js';
import './page-jquery.js';
import './page-03.js';
import './page-04.js';
import './page-05.js';
import './page-06.js';
import './page-07.js';
import './page-08.js';
import './page-09.js';
import './page-10.js';
import './page-11.js';
import './page-12.js';
import './page-13.js';
import './page-14.js';
import './page-15.js';
import './page-16.js';
import './page-17.js';
import './page-18.js';
import './page-19.js';
import './page-20.js';
import './page-21.js';
import './page-23.js';
import './page-es6.js';
import './page-30.js';
import './page-31.js';
import './page-32.js';
import './page-33.js';
import './page-es6modules.js';
import './page-40.js';
import './page-41.js';
import './page-50.js';

export default {
  build() {
    return contents.tagNames
      .map(tagName => document.createElement(tagName));
  },
};
