import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  old1: `
var obj = {
  init: function () {
    var that = this;

    document.querySelector('.sample07 .old1 .run')
      .addEventListener('click', function () {
        // this はイベントの発生元なので
        // これは動かない
        // this.show();
        that.show();
      }); 
  },
  show() {
    alert('old style');
  }
};

obj.init();
`.trim(),
  new1: `
var obj = {
  init: function () {
    document.querySelector('.sample07 .new1 .run')
      .addEventListener('click', () => {
        this.show();
      });
      
      // 一行の時は {} を省略する書き方も
      // .addEventListener('click', () => this.show());
  },
  show() {
    alert('Arrow Function');
  },
};

obj.init();
`.trim(),
};

codes.html2 = codes.html;
codes.html4 = codes.html3;
codes.html6 = codes.html5;

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];

        // eslint-disable-next-line
        eval(codes[type]);
      });
  }

  template() { // eslint-disable-line
    return `
<style>
</style>

<h3>Native JavaScript (Arrow Function)</h3>

<div class="sample07">
  <div class="sample">
    <div class="code old1" data-type="old1">
      <h5 class="title">Native JavaScript (昔)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code new1" data-type="new1">
      <h5 class="title">Native JavaScript (Arrow Function)</h5>
      <div class="navigation">
        <button class="run">実行</button>
      </div>
      <pre><code class="javascript inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
