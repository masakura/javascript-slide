import contents from './content-collection.js';

contents.define(`
<h3>これで webpack が必要なく...</h3>
<ul>
  <li>圧縮は gzip 圧縮でなんとか</li>
  <li>結合は HTTP/2.0 で解決 (するとされている)</li>
  <li>Babel / TypeScript / JSX / vue からのトランスパイルは無理...</li>
  <li>画像や CSS のインポートも無理...</li>
  <li>webpack (もしくは同種のもの) はまだまだ必要</li>
</ul>
`);
