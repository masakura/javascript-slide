import contents from './content-collection.js';

contents.define(`
<style>
  .title-page {
    position: relative;
    width: 100%;
    height: 100%;
  }
  
  .title-page h1 {
    position: absolute;
    margin-bottom: 0;
    width: 100%;
    height: 80%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 200%;
  }
  
  .title-page footer {
    position: absolute;
    width: 100%;
    bottom: 0;
    text-align: right;
  }
</style>

<div class="title-page">
  <h1>JavaScript, without Library</h1>
  
  <footer>
    <p>ゆるやかな合同勉強会 (2018/11/17)</p>
    <address>Tomo Masakura (html5j 鹿児島)</address>
  </footer>
</div>
`);
