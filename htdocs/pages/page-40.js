import contents from './content-collection.js';
import PageContentElement from './page-content.js';

const codes = {
  html1: `
<script src="foo.js"></script>
<script src="bar.js"></script>
`.trim(),
  script1: `
class Foo {
  show() {
    alert('hello!');
  }
}
`.trim(),
  script2: `
const foo = new Foo();
foo.show();
`.trim(),
  new1: `
const array = [1, 2, 3, 4, 5];
array.text = 10;

let total = 0;

for (const value of array) {
  total += value;
}

alert(total);
`.trim(),
  html2: `
<script src="bar.js" type="module"></script>
`.trim(),
  script3: `
export default class Foo {
  show() {
    alert('hello!');
  }
}
`.trim(),
  script4: `
import Foo from 'foo.js';

const foo = new Foo();
foo.show();
`.trim(),
};

class Page extends PageContentElement {
  initialize() {
    this.querySelectorAll('.code')
      .forEach((code) => {
        const $code = code.querySelector('.inner');
        const type = code.dataset.type;

        $code.textContent = codes[type];
      });
  }

  template() { // eslint-disable-line
    return `
<style>
  .samplee .sample > * {
    flex-grow: unset;
  }
  
  .samplee .sample .foo {
    width: 20em;
  }

  .samplee .sample .bar {
    width: 16em;
  }
  
  .samplee .sample .html {
    flex-grow: 1;
  }
</style>

<h3>Native JavaScript (ES6 Modules)</h3>

<p>
  ようやく使えるようになりました!
</p>
<div class="samplee">
  <div class="sample">
    <div class="code foo" data-type="script1">
      <h5 class="title">foo.js</h5>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code bar" data-type="script2">
      <h5 class="title">bar.js</h5>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code html" data-type="html1">
      <h5 class="title">HTML</h5>
      <pre><code class="html inner"></code></pre>
    </div>
  </div>
  <div class="sample">
    <div class="code foo" data-type="script3">
      <h5 class="title">foo.js</h5>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code bar" data-type="script4">
      <h5 class="title">bar.js</h5>
      <pre><code class="javascript inner"></code></pre>
    </div>
    <div class="code html" data-type="html2">
      <h5 class="title">HTML</h5>
      <pre><code class="html inner"></code></pre>
    </div>
  </div>
</div>
`;
  }
}

contents.define(Page);
