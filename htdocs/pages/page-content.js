const getCustomElement = (template) => {
  if ((typeof template).toLowerCase() !== 'string') return template;

  return class Page extends PageContentElement {
    template() { // eslint-disable-line
      return template;
    }
  };
};

export default class PageContentElement extends HTMLElement {
  connectedCallback() {
    this.innerHTML = this.template().trim();

    this.initialize();
  }

  initialize() { // eslint-disable-line
  }

  template() { // eslint-disable-line
    return '';
  }

  static define(tagName, template) {
    const Page = getCustomElement(template);

    customElements.define(tagName, Page);
  }
}
