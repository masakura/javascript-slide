import contents from './content-collection.js';

contents.define(`
<h3>要素の選択</h3>
<p>
  Web の歴史はこういうことの繰り返しみたいなもん。
</p>

<ul>
  <li>
    ユースケース駆動で高レベル API を定義する。  
  </li>
  <li>
    時代が過ぎてユースケースが変わって力不足が発覚する。  
  </li>
  <li>
    力不足を補うために低レベル API を追加する。    
  </li>
  <li>
    面倒なのでライブラリが発達する。  
  </li>
  <li>
    有用なものを JavaScript の世界に取り込む。
  </li>
</ul>
`);
