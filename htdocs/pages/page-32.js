import contents from './content-collection.js';

contents.define(`
<h3>ES2017</h3>
<ul>
  <li><a href="https://github.com/tc39/proposal-string-pad-start-end">Object.values/Object.entries</a></li>
  <li><a href="https://github.com/tc39/proposal-object-getownpropertydescriptors">Object.getOwnPropertyDescriptors</a></li>
  <li><a href="https://github.com/tc39/proposal-trailing-function-commas">Trailing commas in function parameter lists and calls</a></li>
  <li><a href="https://github.com/tc39/ecmascript-asyncawait">Async functions</a></li>
  <li><a href="https://github.com/tc39/ecmascript_sharedmem">Shared memory and atomics</a></li>
</ul>
`);
