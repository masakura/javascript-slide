import contents from './content-collection.js';

contents.define(`
<h3>イベントの登録</h3>
<ol>
  <li>
      初期の JavaScript はイベントを一つしか登録できなかった。複数のイベントを登録するときは頑張ってた。
  </li>
  <li>
    jQuery などが複数のイベントの登録をサポート。
  </li>
  <li>
    ユースケースが認められたので、<code>attachEvent</code> (IE)・<code>addEventListener</code> (DOM Level 2) が登場。
  </li>
  <li>
    jQuery もこれらを利用するようになる。
  </li>
  <li>
    IE が DOM 標準の <code>addEventListener</code> をサポート。
  </li>
  <li>
    jQuery が古いブラウザーのサポートを終了する。
</li>
</ol>
`);
