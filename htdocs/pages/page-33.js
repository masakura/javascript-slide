import contents from './content-collection.js';

contents.define(`
<h3>ES2018</h3>
<ul>
  <li><a href="https://github.com/tc39/proposal-object-rest-spread">Rest/Spread Properties</a></li>
  <li><a href="https://github.com/tc39/proposal-regexp-lookbehind">RegExp Lookbehind Assertions</a></li>
  <li><a href="https://github.com/tc39/proposal-regexp-unicode-property-escapes">RegExp Unicode Property Escapes</a></li>
  <li><a href="https://github.com/tc39/proposal-promise-finally">Promise.prototype.finally</a></li>
  <li><a href="https://github.com/tc39/proposal-async-iteration">Asynchronous Iteration</a></li>
</ul>
`);
