import contents from './content-collection.js';

contents.define(`
<h3>ES2019 (予定)</h3>
<ul>
  <li><a href="https://github.com/tc39/proposal-optional-catch-binding">Optional catch binding</a></li>
  <li><a href="https://github.com/tc39/proposal-json-superset">JSON superset</a></li>
  <li><a href="https://github.com/tc39/proposals#stage-3">Stage 3</a> の中からいくつか</li>
</ul>
`);
