* 短く書くために
  * DOM Level 0 -> getElementBy -> jQuery -> querySelector の流れで、ユースケース駆動に失敗して、低レベル API を追加して、それを補完するライブラリが表れるというのを解説する
  * ネイティブ実装したことで、jQuery も querySelector を利用して高速化されている
  * Extensible Web
* クロスブラウザーのために
  * 昔はブラウザーごとの差が大きかった
  * イベントの追加は、DOM Level 0 / attachEvent (IE) / addEventListener (DOM) の三種類がある
  * Ajax 通信用
  * ブラウザーごとの差を吸収するために、jQuery は非常に有用だった
  * 独自路線を歩んだ Netscape Navigator (今の Firefox の元) や IE も今や標準路線
  * ブラウザーの差は昔と比べるとなくなっていると考えてよい
* jQuery は不要になったのか?
  * querySelectorAll の戻り値は NodeList なので jQuery ほど使い勝手はよくない
  * イベントの登録はすごい強力
  * クロスブラウザーの件はどちらかというと Polyfill やトランスパイラーの役割に
  * アニメーションは CSS アニメーションにするのをお勧めする
  * 昔ほど重要ではなくなってるのは間違いない
  * 自分は使わないか、Vue.js を使うかの二択になっている
* Closure Library (継承関係)
* webpack
* API
  * WebRTC
  * WebAudio
  * WebGL
  * WebBluetooth
  * Web Authentication API
  * Service Worker
